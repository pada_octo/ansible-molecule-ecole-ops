# INSTALLATION

# Windows

Installation de vagrant et virtualbox ([lien vers la doc d'install](https://www.vagrantup.com/docs/installation))

A la racine du répo lancer la commande
```bash
vagrant up
vagrant ssh
```

Vous devriez être connecté à votre machine vagrant en tant qu'utilisateur `vagrant`.
```bash
vagrant@vagrant:~$ lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 20.04.2 LTS
Release:        20.04
Codename:       focal

```

```bash
# One liner à copier-coller pour lancer toute l'installation.
sudo apt -y update && sudo apt -y install python3-pip && sudo pip install ansible==2.10.4 && PATH=${PATH}:/home/vagrant/.local/bin ansible-playbook ./ansible-molecule-tdd/install.yml
```
Installer python3, pip et docker :
```bash
vagrant@vagrant:~$ sudo apt update
vagrant@vagrant:~$ sudo apt install python3-pip
# vagrant@vagrant:~$ sudo apt install docker.io
# vagrant@vagrant:~$ sudo usermod -aG docker vagrant
# Se deconnecter et se reconnecter à la machine virtuelle pour réévaluer le groupe de l'utilisateur courant vagrant
#vagrant@vagrant:~$ pip --version
#pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
```

Installer ansible :
vagrant@vagrant:~$ pip --version
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)

```bash
vagrant@vagrant:~$ cd ansible-molecule-tdd
vagrant@vagrant:~$ pip install -r requirements.txt
vagrant@vagrant:~$ echo "export PATH=${PATH}:/home/vagrant/.local/bin" >> .bashrc
```

Votre environnement de travail est prêt.

```bash
vagrant@vagrant:~$ ansible --version
vagrant@vagrant:~$ molecule --version
vagrant@vagrant:~$ docker --version
```

# Linux / macos


